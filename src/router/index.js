import { createRouter, createWebHistory } from 'vue-router'
import MainPage from '@/pages/MainPage.vue'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      name: 'main',
      path: '/',
      component: MainPage,
    },
    // {
    //   name: 'notfound',
    //   path: '/',
    //   component: MainPage,
    // }
  ]
})
export default router